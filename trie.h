#ifndef trie_h
#define trie_h

/* trie interface */

struct trie_node {
	char value;
	struct pointerlist* index_list;
	struct trie_node* next;
	struct trie_node* prev;
	struct trie_node* parent;
	struct trie_node* child;
	bool is_word;
};

struct trie {
	struct trie_node * root;
};

struct trie_node* new_node(char letter);
struct trie* new_trie();

struct pointerlist* get_photo_index_list(struct trie* trie, char* word);

struct trie_node* trie_search(struct trie_node* the_node, char l);
bool trie_search_word(struct trie* trie, char* word);

struct trie_node * trie_add_word(struct trie* trie, char* word);

#endif /* trie_h */