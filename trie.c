#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "linkedlist.h"
#include "pointerlist.h"
#include "trie.h"


struct trie_node* new_node(char letter)
{
    struct trie_node* node = malloc(sizeof(struct trie_node));
 
    node->value = letter;
    node->index_list = pointerlist_new();
    node->is_word = false;
    node->prev= NULL;
    node->next = NULL;
    node->parent= NULL;
    node->child = NULL;
    return node;
}


struct trie* new_trie()
{
	struct trie* new_trie = malloc(sizeof(struct trie));
	new_trie->root = new_node('\0');
	return new_trie;
}


bool has_next(struct trie_node * the_node)
{
	return the_node->next != NULL;
}


struct trie_node* trie_search(struct trie_node* the_node, char l)
{
	if(the_node->value == l)
		return the_node;
	else{
		if(has_next(the_node))
			the_node = trie_search(the_node->next, l);
	}
	return the_node;
}


struct pointerlist* get_photo_index_list(struct trie* trie, char* word)
{
	struct trie_node * current = trie->root->child;
	if(current==NULL)
		return NULL;
	if(current->value != '\0'){
		struct trie_node * node_search;
		for(int i=0;i<strlen(word);i++){
			node_search = trie_search(current, word[i]);
			if(node_search->value == word[i])
				current = node_search->child;
			else
				return NULL;
		}
		return current->parent->index_list;
	}
	return NULL;
}


bool trie_search_word(struct trie* trie, char* word)
{
	struct trie_node * current = trie->root->child;

	if(current->value != '\0'){
		struct trie_node * node_search;
		//printf("word length: %zu\n", strlen(word));
		for(int i=0;i<strlen(word);i++){
			//printf("word[i]: %c\n", word[i]);
			node_search = trie_search(current, word[i]);
			//printf("parent: %c\n", current->parent->value);
			//printf("current: %c\n", current->value);
			//printf("node_search: %c\nword i: %c\n", node_search->value, word[i]);
			if(node_search->value == word[i])
				current = node_search->child;
			else
				return false;
		}
		return current->parent->is_word;
	}
	return false;
}


struct trie_node * trie_add_word(struct trie* trie, char* word)
{
	struct trie_node * current = trie->root;
	if(trie->root->child==NULL || !trie_search_word(trie, word)){
		//printf("palavra nao encontrada\n");
		if(current->child != NULL){
			current = current->child;
			struct trie_node * node_search;
			for(int i=0;i<strlen(word);i++){
				//printf("word[i]: %c\n", word[i]);
				node_search = trie_search(current, word[i]);
				//printf("node_search: %c\n", node_search->value);
				if(node_search->value == word[i]){
					current = node_search->child;
					//printf("node_search->child: %c\n", current->value);
				}
				else{
					if(node_search->value == '\0'){
						//printf("if \\0\n");
						for(int j=i;j<strlen(word);j++){
							//printf("word[j]: %c\n", word[j]);
							//printf("current: %c\n", current->value);
							current = new_node(word[j]);
							//printf("current: %c\n", current->value);
							//printf("node_search->parent: %c\n", node_search->parent->value);
							//printf("node_search->parent->is_word: %d\n", node_search->parent->is_word);
							current->parent = node_search->parent;
							current->parent->child = current;
						}
					}
					else{
						//printf("else\n");
						//printf("node_search: %c\n", node_search->value);
						node_search->next = new_node(word[i]);
						//printf("node_search->next: %c\n", node_search->next->value);
						node_search->next->prev = current;
						//printf("node_search->next->prev: %c\n", node_search->next->prev->value);
						current = node_search->next;
						current->parent = node_search->parent;
						//printf("current: %c\n", current->value);
						for(int j=i+1;j<strlen(word);j++){
							//printf("word[j]: %c\n", word[j]);
							//printf("current: %c\n", current->value);
							current->child = new_node(word[j]);
							//printf("current->child: %c\n", current->child->value);
							current->child->parent = current;
							//printf("current->child->parent: %c\n", current->child->parent->value);
							current = current->child;
						}
					}
					//printf("---\n");
					current->is_word=true;
					//printf("current: %c\n", current->value);
					//printf("current->is_word: %d\n", current->is_word);
					current->child = new_node('\0');
					//printf("current->child: %c\n", current->child->value);
					//printf("current->child->is_word: %d\n", current->child->is_word);
					current->child->parent = current;
					//printf("current->parent: %c\n", current->parent->value);
					//printf("current->parent->is_word: %d\n", current->parent->is_word);
					if(current->prev != NULL){
					//printf("current->prev: %c\n", current->prev->value);
					//printf("current->prev->next: %c\n", current->prev->next->value);
					}
					break;
				}
			} //end for
		}
		else{
			for(int i=0;i<strlen(word);i++){
				current->child = new_node(word[i]);
				current->child->parent = current;
				current = current->child;
			}
			current->is_word=true;
			current->child = new_node('\0');
			current->child->parent = current;
		}
	}
	else{
		current = current->child;
		//printf("palavra existe\n");
		struct trie_node * node_search;
		for(int i=0;i<strlen(word);i++){
			node_search = trie_search(current, word[i]);
			current = node_search->child;
		}
		//printf("+ current: %c\n", current->value);
		current=current->parent;
		//printf("++ current: %c\n", current->value);
		//printf("current final: %c\n", current->value);
		//printf("current->parent final: %c\n", current->parent->value);
	}

	return current;
}