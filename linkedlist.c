#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "linkedlist.h"


struct list* list_new()
{
	struct list* newlist=malloc(sizeof(struct list));
	newlist->head=NULL;
	return newlist;
}


struct list_node* next(struct list_node* current)
{
	return current->next;
}


bool list_empty(struct list* list)
{
	return list->head==NULL;
}


struct list_node * list_insert_node(struct list_node* head, char title[], int p_id)
{
	struct list_node* new_node = malloc(sizeof(struct list_node));
	strcpy(new_node->photo,title);
	new_node->photo_id=p_id;
	new_node->next=head->next;
	head->next=new_node;
	return new_node;
}


struct list_node * list_insert(struct list *list, char title[], int p_id)
{
	if(list_empty(list)){
		list->head=malloc(sizeof(struct list_node));
		strcpy(list->head->photo,title);
		list->head->photo_id=p_id;
		return list->head;
	}
	else
		return list_insert_node(list->head, title, p_id);
}


void list_print(struct list *list)
{
	struct list_node* current=list->head;
	while(current!=NULL){
		printf("#%d %s ", current->photo_id, current->photo);
		current=next(current);
		if(current!=NULL)
			printf("| ");
	}
	printf("\n");
}


int list_length(struct list *list)
{
	if(list_empty(list))
		return 0;
	else{
		int counter=0;
		struct list_node* current=list->head;
		while(current!=NULL){
			counter++;
			current=next(current);
		}
		return counter;
	}
}

void list_destroy(struct list *list)
{
	if(!list_empty(list)){
		struct list_node* current=list->head;
		while(current!=NULL){
			struct list_node* temp;
			temp=next(current);
			free(current);
			current=temp;
		}
		free(list);
	}
}