#ifndef linkedlist_h
#define linkedlist_h

/* linkedlist interface */

struct list_node{
	struct list_node* next;
	int photo_id;
	char photo[50];
};

struct list{
	struct list_node* head;
};

struct list* list_new();

struct list_node* next(struct list_node* current);

bool list_empty(struct list* list);

void list_destroy(struct list* list);

struct list_node * list_insert_node(struct list_node* head, char title[], int p_id);
struct list_node * list_insert(struct list *list, char title[], int p_id);

void list_print(struct list *list);

int list_length(struct list *list);

void list_destroy(struct list* list);

#endif /* linkedlist_h */