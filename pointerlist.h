#ifndef pointerlist_h
#define pointerlist_h

/* pointerlist interface */

struct pointer_node{
	struct pointer_node* next_pointer;
	struct list_node * photo_node_pointer;
};

struct pointerlist{
	struct pointer_node* head;
	struct pointer_node* tail;
};


struct pointerlist* pointerlist_new();

struct pointer_node* nextpointer(struct pointer_node* current);

bool pointerlist_empty(struct pointerlist* list);

void pointerlist_insert_node(struct pointerlist* list, struct list_node * pointer);
void pointerlist_insert(struct pointerlist* list, struct list_node * pointer);

void pointerlist_print(struct pointerlist* list);

int pointerlist_length(struct pointerlist* list);

void pointerlist_destroy(struct pointerlist* list);

#endif /* pointerlist_h */