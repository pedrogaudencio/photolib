#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "linkedlist.h"
#include "pointerlist.h"


struct pointerlist* pointerlist_new()
{
	struct pointerlist* newlist=malloc(sizeof(struct pointerlist));
	newlist->head=NULL;
	newlist->tail=NULL;
	return newlist;
}


struct pointer_node* nextpointer(struct pointer_node* current)
{
	return current->next_pointer;
}


bool pointerlist_empty(struct pointerlist* list)
{
	return list->head==NULL;
}


void pointerlist_insert_node(struct pointerlist* list, struct list_node * pointer)
{
	struct pointer_node* new_node = malloc(sizeof(struct pointer_node));
	new_node->photo_node_pointer=pointer;
	list->tail->next_pointer=new_node;
	list->tail=new_node;
	//printf("inseriu: %s\n", new_node->photo_node_pointer->photo);
}


void pointerlist_insert(struct pointerlist* list, struct list_node * pointer)
{
	if(pointerlist_empty(list)){
		list->head=malloc(sizeof(struct pointer_node));
		list->head->photo_node_pointer=pointer;
		list->tail=list->head;
		//printf("inseriu h: %s\n", list->head->photo_node_pointer->photo);
	}
	else
		pointerlist_insert_node(list, pointer);
}


void pointerlist_print(struct pointerlist* list)
{
	if(!pointerlist_empty(list)){
		struct pointer_node* current=list->head;
		while(current!=NULL){
			printf("%s\n", current->photo_node_pointer->photo);
			current=nextpointer(current);
		}
	}
}


int pointerlist_length(struct pointerlist* list)
{
	if(pointerlist_empty(list))
		return 0;
	else{
		int counter=0;
		struct pointer_node* current=list->head;
		while(current!=NULL){
			counter++;
			current=nextpointer(current);
		}
		return counter;
	}
}

void pointerlist_destroy(struct pointerlist* list)
{
	if(!pointerlist_empty(list)){
		struct pointer_node* current=list->head;
		while(current!=NULL){
			struct pointer_node* temp;
			temp=current->next_pointer;
			free(current);
			current=temp;
		}
		free(list);
	}
}