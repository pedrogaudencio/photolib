all: compile run

compile:
	gcc linkedlist.c pointerlist.c trie.c photolib.c -Wall -std=c99 -lm -o photolib

run:
	./photolib

clean:
	rm *.o photolib
