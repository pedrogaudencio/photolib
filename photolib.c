#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "linkedlist.h"
#include "pointerlist.h"
#include "trie.h"

int n=1;


struct list_node * add_photo(struct list * photolist, char * photo)
{
	return list_insert(photolist, photo, n); // adiciona foto à lista de fotos com um máximo de 50 caracteres e retorna o apontador do nó da foto
}


void add_topic(struct trie * topic_collection, char * topic, struct list_node * photo_node_pointer)
{
	struct trie_node * the_node = trie_add_word(topic_collection, topic); // adiciona os tema à trie
	pointerlist_insert(the_node->index_list, photo_node_pointer); // adiciona apontador da foto à lista de apontadores do tema
}


struct pointerlist * intersect_collections(struct pointerlist * this_list, struct pointerlist * that_list)
{
	/*
		quando se inserirem as fotos na lista, vão ser inseridas por ordem de id, então quando se fizer a pesquisa usam-se os ids também:
			1. verifica o tamanho das listas
			2. verifica se o id da foto n da lista mais pequena é igual ao da foto n da lista maior
			3. verifica se o id da foto n da lista maior é maior que o da foto n da lista mais pequena
				- se for, significa que a foto não existe na lista e não é comum a ambos os temas e assim pode-se passar logo à foto n+1
	*/

	if(pointerlist_empty(this_list) || pointerlist_empty(that_list))
		return NULL;

	struct pointerlist * intersection = pointerlist_new();
	struct pointerlist * largest_list;
	struct pointerlist * smallest_list;
	struct pointer_node * s_pointer;
	struct pointer_node * l_pointer;

	if(pointerlist_length(that_list)>pointerlist_length(this_list)){
		largest_list=that_list;
		smallest_list=this_list;
		s_pointer=smallest_list->head;
		l_pointer=largest_list->head;
	}
	else{
		largest_list=this_list;
		smallest_list=that_list;
		s_pointer=largest_list->head;
		l_pointer=smallest_list->head;
	}

	int s_length = pointerlist_length(smallest_list);
	int l_length = pointerlist_length(largest_list);

	for(int i=0;i<s_length;i++){
		for(int j=0;j<l_length;j++){

			if(s_pointer->photo_node_pointer->photo_id==l_pointer->photo_node_pointer->photo_id){
				pointerlist_insert(intersection, s_pointer->photo_node_pointer);
				break;
			}
			else
				if(l_pointer->photo_node_pointer->photo_id>s_pointer->photo_node_pointer->photo_id)
					break;

			/*
			printf("i: %d | ", i);
			printf("s_pointer: #%d %s\n", s_pointer->photo_node_pointer->photo_id, s_pointer->photo_node_pointer->photo);
			printf("j: %d | ", j);
			printf("l_pointer: #%d %s\n------\n", l_pointer->photo_node_pointer->photo_id, l_pointer->photo_node_pointer->photo);
			*/

			if(nextpointer(l_pointer)!=NULL)
				l_pointer = nextpointer(l_pointer);
		}
		l_pointer=largest_list->head;
		if(nextpointer(s_pointer)!=NULL)
			s_pointer=nextpointer(s_pointer);
	}

	return intersection;
}


void search_photos(struct pointerlist * topic_list)
{
	if(topic_list != NULL){
		printf("+ encontrada(s) %d fotografia(s)\n", pointerlist_length(topic_list));
		pointerlist_print(topic_list);
	}
	else
		printf("+ encontrada(s) %d fotografia(s)\n", 0);
}


void load_database(char * db, struct list* photolist, struct trie* topic_collection, struct list_node * node_photo){

	FILE * file=fopen(db,"r");
	if(file==NULL)
		return;
	else
		fclose(file);
		
	file=fopen(db,"r+");

	int buffer=500;
	char * com = malloc(sizeof(char) * 4096);

	while(1)
	{
		fgets(com,buffer,file);
		com = strtok(com,"\n");

		int pos = (int) fgetc(file);

		if(pos == EOF)
			break;
		
		ungetc(pos, file);

		char * photo_name=malloc(sizeof(char) * 50);
		char * photo_name2=malloc(sizeof(char) * 50);
		char * photo_name3=malloc(sizeof(char) * 50);
		photo_name = com;
		photo_name2 = strtok(photo_name," ");

		if(strcmp(photo_name2,"I")==0){

			photo_name3=strtok(NULL,"\n");
			node_photo = add_photo(photolist, photo_name3);
			n++;

			while(1)
			{
				fgets(com,buffer,file);
				com=strtok(com,"\n");

				if(strcmp(com,".")==0)
					break;

				add_topic(topic_collection, com, node_photo);
			}
		}
	}
}


int main()
{
	struct list* photolist = list_new();
	struct trie* topic_collection = new_trie();
	struct list_node * node_photo = NULL;

	int buffer=500;
	char * to_save = malloc(sizeof(char)*100000000);
	char * com = malloc(sizeof(char) * 4096);
	memset(to_save,0,sizeof(char)*100000000);
	memset(com,0,sizeof(char)*4096);

	load_database("db.txt", photolist, topic_collection, node_photo);

	while(1)
	{
		fgets(com,buffer,stdin);

		if(strcmp(com,"P\n")!=0)
			//to_save = (char*) realloc(to_save, sizeof(to_save) + 50 * sizeof(char));
			strcat(to_save,com);
		com = strtok(com,"\n");

		if(strcmp(com,"X")==0){
			//to_save = (char*) realloc(to_save, sizeof(to_save) + 50 * sizeof(char));
			strtok(to_save,"X");
			break;
		}

		char * photo_name=malloc(sizeof(char) * 50);
		char * photo_name2=malloc(sizeof(char) * 50);
		char * photo_name3=malloc(sizeof(char) * 50);
		memset(photo_name,0,sizeof(char)*50);
		memset(photo_name2,0,sizeof(char)*50);
		memset(photo_name3,0,sizeof(char)*50);
		photo_name = com;
		photo_name2 = strtok(photo_name," ");

		if(strcmp(photo_name2,"I")==0){

			photo_name3=strtok(NULL,"\n");

			node_photo = add_photo(photolist, photo_name3);
			printf("+ fotografia %d introduzida\n", n);
			n++;

			while(1)
			{
				fgets(com,buffer,stdin);
				//to_save = (char*) realloc(to_save, sizeof(to_save) + 50 * sizeof(char));
				strcat(to_save,com);
				com=strtok(com,"\n");

				if(strcmp(com,".")==0)
					break;

				add_topic(topic_collection, com, node_photo);
			}
		}
		else{ // pesquisa
			fgets(com,buffer,stdin);
			com = strtok(com,"\n");
			struct pointerlist * intersected = NULL;
			if(get_photo_index_list(topic_collection, com)==NULL){
				while(1)
				{
					fgets(com,buffer,stdin);
					com = strtok(com,"\n");
					if(strcmp(com,".")==0)
				    	break;
				}
			}
			else{
				intersected = get_photo_index_list(topic_collection, com);
				while(1)
				{
					fgets(com,buffer,stdin);
					com = strtok(com,"\n");
					if(strcmp(com,".")!=0)
				    	intersected = intersect_collections(intersected, get_photo_index_list(topic_collection, com));
					if(strcmp(com,".")==0)
				    	break;
				}
			}
			search_photos(intersected);
		}
	}

	FILE *file=fopen("db.txt","a+");
	fprintf(file,"%s",to_save);

	fclose(file);

	list_destroy(photolist);

	return 0;

}